import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-holamundo',
  templateUrl: './holamundo.page.html',
  styleUrls: ['./holamundo.page.scss'],
})
export class HolamundoPage implements OnInit {

  name: string;
  age: string;
  gender: string;
  constructor(public alertController: AlertController) { }

  ngOnInit() {
  }

  async processData() {
    let msg: string;
    if(!this.name || !this.gender || !this.age){
      msg = `Favor de llenar los campos: <br>
        ${!this.name ? 'Nombre <br>':''}
        ${!this.gender ? 'Genero <br>':''}      
        ${!this.age ? 'Edad <br>':''}
        `;
    }else{
      msg = `Bienvenido: ${this.name} <br> De sexo: ${this.gender} <br>  De edad: ${this.age}`;
    }
    const alert = await this.alertController.create({ 
      cssClass: 'cus-class',
      header: 'Hola Mundo',
      message: msg,
      buttons: ['Aceptar']
    });

    await alert.present();
  }

  cleanData(){
    this.age = '';
    this.gender = '';
    this.name = '';
  }
}
