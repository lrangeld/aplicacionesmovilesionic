import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { ContactoService } from 'src/app/services/contacto.service';

@Component({
  selector: 'app-contactos',
  templateUrl: './contactos.page.html',
  styleUrls: ['./contactos.page.scss'],
})
export class ContactosPage implements OnInit {
  constructor( private contactoService: ContactoService, public alertController: AlertController) { 
    this.getContacts();
  }

  contacts = new Array();

  ngOnInit() {
  }

  getContacts(){
    this.contactoService.getAllContacts().subscribe(
      res => {
        this.contacts = res.map( e => {
          return {...e, urlImage: `https://picsum.photos/id/${e.id}/70/70`};
        });
      }
    );
  }

  async showInfo(contact) {
    let msg: string = `Calle: ${contact.address.street} <br>Ciudad: ${contact.address.city} <br>Codigo Postal: ${contact.address.zipcode}`
    const alert = await this.alertController.create({
      header: contact.name,
      subHeader: 'Direccion:',
      cssClass: 'cus-class',
      message: msg,
      buttons: ['OK']
    });

    await alert.present();
  }
}
