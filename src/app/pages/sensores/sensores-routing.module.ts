import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SensoresPage } from './sensores.page';

const routes: Routes = [
  {
    path: '',
    component: SensoresPage
  },
  {
    path: 'gps',
    loadChildren: () => import('./gps/gps.module').then( m => m.GpsPageModule)
  },
  {
    path: 'acelerometro',
    loadChildren: () => import('./acelerometro/acelerometro.module').then( m => m.AcelerometroPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SensoresPageRoutingModule {}
