import { Component, OnInit } from '@angular/core';
import { DeviceMotion, DeviceMotionAccelerationData } from '@ionic-native/device-motion/ngx';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-acelerometro',
  templateUrl: './acelerometro.page.html',
  styleUrls: ['./acelerometro.page.scss'],
})
export class AcelerometroPage implements OnInit {

  private Xaxis: number = 0;
  private Yaxis: number = 0;
  private Zaxis: number = 0;
  private start: boolean = false;
  
  constructor(private deviceMotion: DeviceMotion) { }
  private subscription
  ngOnInit() {
  }

  getData(){
    this.start = true;
    // Get the device current acceleration
    this.deviceMotion.getCurrentAcceleration().then(
      (acceleration: DeviceMotionAccelerationData) => console.log(acceleration),
      (error: any) => console.log(error)
    );

    // Watch device acceleration
    this.subscription = this.deviceMotion.watchAcceleration().subscribe((acceleration: DeviceMotionAccelerationData) => {
      console.log(acceleration);
      this.Xaxis = acceleration.x;
      this.Yaxis = acceleration.y;
      this.Zaxis = acceleration.z;
    });
  }


  stopData(){
    this.start = false;
  // Stop watch
  this.subscription.unsubscribe();
}

}
