import { Component, OnInit } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { ToastController, Platform} from '@ionic/angular';
import { GoogleMap, GoogleMaps, Environment, Marker, GoogleMapsEvent, GoogleMapOptions, MarkerOptions } from "@ionic-native/google-maps";

@Component({
  selector: 'app-gps',
  templateUrl: './gps.page.html',
  styleUrls: ['./gps.page.scss'],
})

export class GpsPage implements OnInit {

  map: GoogleMap;
  
  constructor(private platform: Platform, public toastController: ToastController, private geolocation: Geolocation, public googleMaps: GoogleMaps) {  }

  async ngOnInit() {
    await this.platform.ready();
    this.loadMap();
  }

  getPos(){
    this.geolocation.getCurrentPosition().then((resp) => {
      this.setPositionAtMap(resp.coords.latitude, resp.coords.longitude)
      this.showPossition(`Lat: ${resp.coords.latitude}<br>Long: ${resp.coords.longitude}`);
      console.log(`Lat: ${resp.coords.latitude}\nLong: ${resp.coords.longitude}`);
    }).catch((error) => {
      console.log('Error getting location', error);
    });
    
    /*let watch = this.geolocation.watchPosition();
    watch.subscribe((data) => {
      this.setPositionAtMap(data.coords.latitude, data.coords.longitude)
    });*/
  }

  async showPossition(msg: string) {
    const toast = await this.toastController.create({
      header: 'Posicion:',
      message: msg,
      position: 'bottom',
      duration: 5000,
      buttons: [
        {
          side: 'start',
          icon: 'location'
        }
      ]
    });
    toast.present();
  }

  loadMap(){
    let mapOptions: GoogleMapOptions = {
      controls: {
        compass: true,
        //myLocationButton: true,
        zoom: true
      },
      camera: {
        target: {
          lat: 19.519846501655746,
          lng: -96.915034149108
        },
        zoom: 18
      }
    };

    this.map = GoogleMaps.create('map', mapOptions);
    this.map.one(GoogleMapsEvent.MAP_READY).then(async (res) => {
        await this.getPos();
        console.log('Map load');
      }
    ).catch(err => {
        console.log('Error: ' + err);
      });
  }

  setPositionAtMap(lat: number, lng: number){
    this.map.moveCamera({
      target: {
        lat: lat,
        lng: lng
      }
    });

    this.map.addMarker({
      title: 'Mi posicion actual',
      icon: 'red',
      animation: 'DROP',
      position: {
        lat: lat,
        lng: lng
      }
    });
  }
}
