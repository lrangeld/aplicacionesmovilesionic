import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-sensores',
  templateUrl: './sensores.page.html',
  styleUrls: ['./sensores.page.scss'],
})
export class SensoresPage implements OnInit {

  constructor(private router: Router) { }

  private acelerometer: number = 0;
  private gps: number = 1;

  ngOnInit() {
  }


  goToPage(pageType: number){
    if(pageType == this.gps){
      this.router.navigate(['/gps']);
    }else{
      this.router.navigate(['/acelerometro']);      
    }
  }
}
