import { Component, OnInit } from '@angular/core';
import { AlertController, ToastController } from '@ionic/angular';
import { Rating } from 'src/app/models/rating.model';
import { RatingsDbService } from '../../services/ratings.db.service';

@Component({
  selector: 'app-calificaciones',
  templateUrl: './calificaciones.page.html',
  styleUrls: ['./calificaciones.page.scss'],
})
export class CalificacionesPage implements OnInit {

  constructor(public alertController: AlertController, public toastController: ToastController, private ratingsService: RatingsDbService) { 
  }

  ratings = new Array();
  btn_position: string;

  ngOnInit() {

    this.ratingsService.getDBStatus().subscribe((resp)=>{
      if(resp){
        //this.showMsg('Db is ready');
        this.getRatings();
      }/*else{
        this.showMsg('App can\'t read Database');
      }*/
    });

  }

  //Metodo para ingresar los datos de la calificación a agregar
  async showInputRating() {
    const alert = await this.alertController.create({
      cssClass: 'cus-class',
      header: 'Datos de la evaluación',
      inputs: [
        {
          name: 'week',
          type: 'number',
          placeholder: 'No. de semana'
        },
        {
          name: 'activity',
          type: 'text',
          placeholder: 'Tipo de actividad'
        },
        {
          name: 'rating',
          type: 'number',
          placeholder: 'Calificación',
          min: 0,
          max: 10
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (data) => {
            this.addRating(data.week, data.activity, data.rating);
          }
        }
      ]
    });

    await alert.present();
  }

  //Metodo para mostra mensajes
  async showMsg(msg: string){
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  }

  //Metodo para la confirmacion de la eliminación del registro
  async delteRecordConfirm(ratingToDelete: Rating) {
    const alert = await this.alertController.create({
      cssClass: 'cus-class',
      header: 'Eliminar elemento',
      message: '¿Desea <strong>eliminar de manera permanente</strong> el registro seleccionado?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          // cssClass: 'secondary',
          handler: () => {
            console.log('Se cancelo la eliminación');
          }
        }, {
          text: 'Sí',
          handler: () => {
            this.deleteRating(ratingToDelete);
          }
        }
      ]
    });

    await alert.present();
  }

  //Metodo para agragar datos de calificación
  addRating(week: number, activity: string, rating: number){
    let insertRating = new Rating(week, activity, rating);
    this.ratingsService.addRating(insertRating).then( resp => {
      this.getRatings();
      this.showMsg('Elemento agregado');
    }).catch((e) => {
      this.showMsg('Error al agregar');
    });
  }

  //Metodo para obtener los datos de la base de datos
  getRatings(){
    this.ratingsService.getAllRatings().then( ratings => {
      this.ratings = ratings;
      this.btn_position = this.ratings.length > 6 ? 'start':'end';
    }).catch(e => {
      console.log('Error al recuperar calificaciones del servicio!\nError:');
      console.log(e);
    });
  }

  //Metodo para la eliminacion del elemento seleccionado
  deleteRating(ratingToDelete: Rating){
    this.ratingsService.deleteRating(ratingToDelete).then((resp) => {
      this.getRatings();
      this.showMsg('Elemento eliminado');
    }).catch((e) => {
      console.log('Error al eliminar registro\nError:');
      console.log(e);
    });
  }
}
