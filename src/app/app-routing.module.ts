import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'bienvenida',
    pathMatch: 'full'
  },
  {
    path: 'helloworld',
    loadChildren: () => import('./pages/holamundo/holamundo.module').then( m => m.HolamundoPageModule)
  },
  {
    path: 'contactos',
    loadChildren: () => import('./pages/contactos/contactos.module').then( m => m.ContactosPageModule)
  },
  {
    path: 'sensores',
    loadChildren: () => import('./pages/sensores/sensores.module').then( m => m.SensoresPageModule)
  },
  {
    path: 'gps',
    loadChildren: () => import('./pages/sensores/gps/gps.module').then( m => m.GpsPageModule)
  },
  {
    path: 'acelerometro',
    loadChildren: () => import('./pages/sensores/acelerometro/acelerometro.module').then( m => m.AcelerometroPageModule)
  },
  {
    path: 'bienvenida',
    loadChildren: () => import('./pages/bienvenida/bienvenida.module').then( m => m.BienvenidaPageModule)
  },  {
    path: 'calificaciones',
    loadChildren: () => import('./pages/calificaciones/calificaciones.module').then( m => m.CalificacionesPageModule)
  }



];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
