import { Component } from '@angular/core';
import { AlertController, MenuController } from '@ionic/angular';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(private menu: MenuController, public alertController: AlertController) {}

  closeMenu() {
    this.menu.close();
  }

  async logOutConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'cus-class',
      header: 'Cerrar aplicación',
      message: '¿Desea <strong>cerrar la aplicación</strong>?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Se cancelo la acción de cerrar la aplicación');
          }
        }, {
          text: 'Sí',
          handler: () => {
            navigator["app"].exitApp();
          }
        }
      ]
    });

    await alert.present();
  }
}
