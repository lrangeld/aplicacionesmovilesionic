import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { Platform } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';
import { Rating } from '../models/rating.model';

@Injectable({
  providedIn: 'root'
})
export class RatingsDbService {

  private data_base: SQLiteObject;
  private dbIsReady = new BehaviorSubject<boolean>(false);

  //Metodo constructor (Crea la DB si no existe, así como la tala correspondiente)
  constructor( private platform: Platform, private sqlite: SQLite) {
    this.platform.ready().then(()=>{
      this.sqlite.create({
        name: 'ratings_db',
        location: 'default'
      }).then((db: SQLiteObject)=>{
        this.data_base = db;
        this.createTable().then(()=>{
          this.dbIsReady.next(true);
        }).catch((e)=>console.log(`Error al preparar base de datos\nCodigo: ${e.code}\nMensaje: ${e.message}`));
      }).catch(()=>console.log('Error al iniciar base de datos'));
    }).catch(()=>console.log('Error al iniciar plataforma'));

  }

  //Metodo para crear tabla 
  private createTable(){
    const query = 'CREATE TABLE IF NOT EXISTS ratings (_id INTEGER PRIMARY KEY AUTOINCREMENT, week INTEGER, activity TEXT, rating INTEGER);';

    return this.data_base.executeSql(query, []);
  }

  //Metodo para obtener el estado de la BD (Esta lista o no)
  public getDBStatus(){
    return this.dbIsReady.asObservable();
  }

  //Metodo para agregar una calificacion a la base de datos
  public addRating(rating: Rating){
    let insertRating = [rating.week, rating.activity, rating.rating];
    const query = 'INSERT INTO ratings(week, activity, rating) VALUES (?, ?, ?);'

    return this.data_base.executeSql(query, insertRating);
  }

  //Metodo para recuperar todas las calificaciones de la base de datos
  public getAllRatings(): Promise<any>{
    const query = 'SELECT * FROM ratings';
    return this.data_base.executeSql(query, []).then(resp =>{
      let ratings_list = [];
      
      for (let index = 0; index < resp.rows.length; index++) {
        ratings_list.push(new Rating(resp.rows.item(index).week, resp.rows.item(index).activity, resp.rows.item(index).rating, resp.rows.item(index)._id));
        
      }
      return ratings_list;
    }).catch((e) => {
      console.log('Error al recuperar todos los registros\n Error: ');
      console.log(e);
    });
  }

  //Metodo para elimar una calificacion de la base de datos
  public deleteRating(rating: Rating){
    let toDeleteRating = [rating._id];
    const query = 'DELETE FROM ratings WHERE _id = ?;'
    return this.data_base.executeSql(query, toDeleteRating);
  }
}
