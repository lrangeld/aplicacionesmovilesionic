import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Contacto } from '../models/contacto.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ContactoService {

  private USER_ENDPOINT: string;
  private IMG_ENDPOINT: string;

  constructor(private http: HttpClient) {
    this.USER_ENDPOINT = environment.usersApi;
    this.IMG_ENDPOINT = environment.imgApi;
  }


  getAllContacts(): Observable<any>{
    return this.http.get<Contacto>(this.USER_ENDPOINT);
  }
  
  getPictures(): Observable<any>{
    return this.http.get(this.IMG_ENDPOINT);
  }
}
