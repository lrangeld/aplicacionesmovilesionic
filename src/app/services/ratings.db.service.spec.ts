import { TestBed } from '@angular/core/testing';

import { Ratings.DbService } from './ratings.db.service';

describe('Ratings.DbService', () => {
  let service: Ratings.DbService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(Ratings.DbService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
