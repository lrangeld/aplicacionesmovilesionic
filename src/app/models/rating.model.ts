export class Rating{
    constructor(week: number, activity: string, rating: number, _id: number = -1){
        this._id = _id;
        this.week = week;
        this.activity = activity;
        this.rating = rating;
    }

    _id: number;
    week: number;
    activity: string;
    rating: number;
}