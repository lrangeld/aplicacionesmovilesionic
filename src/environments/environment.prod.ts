export const environment = {
  production: true,
  usersApi: 'https://jsonplaceholder.typicode.com/users',
  imgApi: 'https://picsum.photos/v2/list?page=2&limit=10',
};
